use netstack::{AfterDispatchState, NetStack, StackContext, StackRespons, init_stack};
use nic::Nic;
use smoltcp::{phy::{Device, DeviceCapabilities, RxToken}, time::{Duration, Instant}, wire::{IpEndpoint, Ipv4Address}};

pub extern crate smoltcp;

pub mod nic;
pub mod netstack;
pub mod shared_loopback;
/*
struct NicReceiver{
    
}

struct NetworkStackInState {
    
}

struct NetworkStackOutState {
    
}

struct NicOut {
    
}

struct Data {
    
}

struct Request {
    
}

struct Response {
    
}

struct NicDataOut {
    
}




fn init_receiver() -> NicReceiver {
    NicReceiver{}
}

fn init_network_stack_in() -> NetworkStackInState {
    NetworkStackInState{}
}

fn init_network_stack_out() -> NetworkStackOutState {
    NetworkStackOutState{}
}

impl NetworkStackInState {
 
    fn into_request(&mut self, _d:Data) -> Request {
        unimplemented!()
    } 
}

impl NetworkStackOutState {
    fn into_data(&mut self, _r:Response) -> Data {
        unimplemented!()
    } 
}

fn init_nic_out() -> NicOut {
    NicOut{}
}


impl Iterator for NicReceiver{
    type Item = Data;
    fn next(&mut self) -> Option<Self::Item> {
        unimplemented!()
    }
}

impl NicOut {
    fn emit(&mut self, _d: NicDataOut) {
        unimplemented!()
    }
}

*/

type Request = Vec<u8>;
type Response = Vec<u8>;

fn init_app() -> App {
    App{}
}
struct App {
    
}
impl App {
    fn process(&mut self, r: Request) -> Response {
        println!("App got request: {}", core::str::from_utf8(&r).unwrap());

	let response = b"ResponsFromServer".to_vec();
	println!("App sending: {}", core::str::from_utf8(&response).unwrap());
	response
    }
}


fn execute(
    app: &mut App,
    new_state: Option<AfterDispatchState>,
    mut nic: Nic,
    mut stack_in: NetStack,
    mut stack_out: NetStack,
    caps: &DeviceCapabilities,
    mut clock: Instant
){
    //Load new state into in if there is one
    if let Some(new) = new_state{
	stack_in.load_after_dispatch_state(new);
	//stack_in.copy_state(&mut stack_out);
    }
    
    //Wait for new packages.
    let (rx_buffer, tx_token) = {
	//Wait for a new package set to be processed
	'wait_loop: loop{
	    if let Some((rx, tx)) = nic.device.receive(){
		//unwrap data of the rx token
		let rx_buffer = rx.consume(clock, |frame| Ok(frame.to_vec())).expect("Failed to unwrap rx");
		break (rx_buffer, tx);
	    }else{
		//println!("Could not get new package, waiting");
		continue 'wait_loop;
	    }
	}
    };

    //process incoming package
    let (new_state, stack_respons) = stack_in.process_ingress(&rx_buffer, clock);
    //Update out part with new state 
    stack_out.load_after_process(new_state);
    //stack_out.copy_state(&mut stack_in);
    
    //Now react to the stack respons
    match stack_respons{
	StackRespons::ControlPackage(p) => {
	    println!("Had control package!");
	    //If we got a control package, send that 
	    let new_state = stack_out.direct_dispatch(tx_token, clock, p);
	    clock += Duration::from_millis(1);
	    //Since we just called dispatch which does not alter the state, just recurse without a new state
	    execute(app, Some(new_state), nic, stack_in, stack_out, caps, clock);	
	},
	StackRespons::NoPackage => {
	    println!("No package");		
	    let new_state = stack_out.process_egress(caps, tx_token, clock);
	    
	    clock += Duration::from_millis(1);
	    
	    execute(app, Some(new_state), nic, stack_in, stack_out, caps, clock);
	},
	StackRespons::FinishedPackage(p) => {
	    println!("Got finished package!");
	    //Had no control package to handle, therefore try to receive a package.	
	    let app_respons = app.process(p);
	    stack_out.on_socket(move |socket| socket.send_slice(&app_respons)).expect("Failed to send data back");
	    let new_state = stack_out.process_egress(caps, tx_token, clock);
	    clock += Duration::from_millis(1);
	    
	    execute(app, Some(new_state), nic, stack_in, stack_out, caps, clock);
	},
	StackRespons::Closed => {
	    //Do not recurse, but break the loop
	    return;
	}
    }
}

///NOTE loopback device only used for testing, later the nic will be created here
pub fn network_application(loopback_device: shared_loopback::Bridge) {    
    println!("Start network app");
    
    //create the nic, currently just a loopback device.
    let nic = nic::Nic::new(loopback_device);
    let capabilities = nic.device.capabilities();
    
    let address = IpEndpoint::new(Ipv4Address([127, 0, 0, 2]).into(), 1337);
    
    let mut in_context = StackContext::new(address);
    let mut out_context = StackContext::new(address);
    
    let (mut stack_in, mut stack_out) = init_stack(
	&mut in_context,
	&mut out_context,
	address
    );
        
    let mut app = init_app();
    let clock = Instant::from_millis(0);

    //Sync socket state
    stack_out.copy_state(&mut stack_in);
    
    println!("---> Finished setup");
    //Start tail recursion
    execute(
	&mut app,
	None,
	nic,
	stack_in,
	stack_out,
	&capabilities,
	clock
    );

    println!("Stopped");
/*
    let mut close = false;

    while !close{
	let (rx_buffer, tx_token) = if let Some((rx, tx)) = nic.device.receive(){
	    //unwrap data of the rx token
	    let rx_buffer = rx.consume(clock, |frame| Ok(frame.to_vec())).expect("Failed to unwrap rx");
	    (rx_buffer, tx)
	}else{
	    continue
	};

	let (new_state, stack_respons) = stack_in.process_ingress(&rx_buffer, clock);
	stack_out.load_after_process(new_state);
	
	match stack_respons{
	    StackRespons::ControlPackage(p) => {
		println!("Had control package!");
		//If we got a control package, send that 
		if let Err(e) = stack_out.iface.inner.dispatch(tx_token, clock, p){
		    println!("NETDEBUG: cannot dispatch response packet: {}", e);
		}
		continue;
	    },
	    StackRespons::NoPackage => {
		println!("No package");
		//Got no package, therefore just process egress.
		stack_out.copy_state(&mut stack_in);		
		stack_out.process_egress(&capabilities, tx_token, clock);
		stack_in.copy_state(&mut stack_out);
	    },
	    StackRespons::FinishedPackage(p) => {
		println!("Got finished package!");
		//Copy state into sender side, cant do this before, because parts of the tx buffer of the in_side might
		//be borrowed when we got a ControlPackage earlier.
		stack_out.copy_state(&mut stack_in);

		//Had no control package to handle, therefore try to receive a package.	
		let app_respons = app.process(p);
		stack_out.on_socket(move |socket| socket.send_slice(&app_respons)).expect("Failed to send data back");
		stack_out.process_egress(&capabilities, tx_token, clock);
		//TODO copy back new state to in socket
		stack_in.copy_state(&mut stack_out);
	    }
	}
	//Advance clock
	clock += Duration::from_millis(1);
    }

    //TODO start polling raw frame from the nic, which should include the close message.
    
    
    /*
    for data in nic {
	
	//non needed anymore, since a dataframe is already unwrapped
	let request = network_stack_in.into_request(data);
	
	let response = app.process(request);
	let out_data = network_stack_out.into_data(response);
	nic_out.emit(data);
    }
    */
*/
}

// fn setup() -> (NicReceiver, NetworkStackInState, App, NetworkStackOutState, NicOut) {
//     ( init_receiver()
//     , init_network_stack_in()
//     , init_app()
//     , init_network_stack_out()
//     , init_nic_out())
// }

// fn network_application2() {
//     let (nic, mut network_stack_in, mut app, mut network_stack_out, mut nic_out) = setup();
    
//     for data in nic {
//         let request = network_stack_in.into_request(data);
//         let response = app.process(request);
//         let out_data = network_stack_out.into_data(socket, response);
//         nic_out.emit(out_data);   
//     }
// }

//fn main() {
//    network_application()
//}

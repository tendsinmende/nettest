use std::sync::{Arc, Mutex};

use smoltcp::phy::{DeviceCapabilities, RxToken, TxToken};
use smoltcp::time::Instant;
use smoltcp::Result;

pub struct Bridge{
    send_buffer: Arc<Mutex<Vec<Vec<u8>>>>,
    receive_buffer: Arc<Mutex<Vec<Vec<u8>>>>
}


pub fn new_bridge() -> (Bridge, Bridge){
    let a = Arc::new(Mutex::new(Vec::new()));
    let b = Arc::new(Mutex::new(Vec::new()));

    (
	Bridge{send_buffer: a.clone(), receive_buffer: b.clone()},
	Bridge{send_buffer: b, receive_buffer: a}
    )
}

impl<'a> smoltcp::phy::Device<'a> for Bridge {
    type RxToken = SLRxToken;
    type TxToken = SLTxToken;

    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {
	if self.receive_buffer.lock().unwrap().len() > 0{
	    //println!("Bridge construct pair");
	    let rx = SLRxToken{data: self.receive_buffer.lock().unwrap().remove(0)};
	    let tx = SLTxToken{target_buffer: self.send_buffer.clone()};

	    Some((rx, tx))
	}else{
	    //println!("Bridge no new pair");
	    None
	}
    }

    fn transmit(&'a mut self) -> Option<Self::TxToken> {
	//println!("Bridge Transmitt");
	Some(SLTxToken{
	    target_buffer: self.send_buffer.clone()
	})
    }

    fn capabilities(&self) -> DeviceCapabilities {
        let mut caps = DeviceCapabilities::default();
        caps.max_transmission_unit = 1536;
        caps.max_burst_size = Some(1);
	caps
    }
}

pub struct SLRxToken{
    data: Vec<u8>
}

impl RxToken for SLRxToken {
    fn consume<R, F>(mut self, _timestamp: Instant, f: F) -> Result<R>
        where F: FnOnce(&mut [u8]) -> Result<R>
    {
	//println!("Bridge consume rxToken");
        let res = f(&mut self.data);
	res
    }
}

pub struct SLTxToken{
    target_buffer: Arc<Mutex<Vec<Vec<u8>>>>
}

impl TxToken for SLTxToken{
    fn consume<R, F>(self, _timestamp: Instant, len: usize, f: F) -> Result<R>
        where F: FnOnce(&mut [u8]) -> Result<R>
    {
	//println!("Bridge consume txToken");
	let mut data = vec![0; len];
	let res = f(&mut data);
	self.target_buffer.lock().unwrap().push(data);
	res
    }
}

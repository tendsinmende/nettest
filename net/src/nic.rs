use crate::shared_loopback::Bridge;

pub struct Nic{
    ///tracks which socket descriptors are taken
    pub device: Bridge,
}

impl Nic{
    ///Creates an abstract NetDevice on `address`
    pub fn new(loopback_device: Bridge) -> Self{	
	Nic{
	    device: loopback_device
	}
    }
}

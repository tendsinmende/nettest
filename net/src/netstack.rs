use smoltcp::{Error, iface::{Neighbor, ethernet::{EthernetPacket, IpPacket}}, phy::{self, DeviceCapabilities, TxToken}, socket::TcpState, wire::{EthernetFrame, IpEndpoint, PrettyPrinter, TcpSeqNumber}};
use smoltcp::iface::EthernetInterface;

use smoltcp::wire::{EthernetAddress, IpAddress, IpCidr};
use smoltcp::iface::{NeighborCache, EthernetInterfaceBuilder};
use smoltcp::socket::{SocketSet, TcpSocket, TcpSocketBuffer};
use smoltcp::time::Instant;
use smoltcp::socket::SocketHandle;
use smoltcp::socket::SocketSetItem;

const NEIGHBOR_CACHE_SIZE: usize = 8;
const TX_BUFFER_SIZE: usize = 1024;
const RX_BUFFER_SIZE: usize = 1024;
///State change after socket.process()
pub struct AfterProcessState{
    //iface state to copy
    neighbor_cache: (Vec<Option<(IpAddress, Neighbor)>>, Instant, usize),
    //TODO rawSocket filter and Igmp path in process_ipv4 not covered
    //shared state
    remote_last_ack: Option<TcpSeqNumber>,
    remote_last_win: u16,
    remote_last_seq: TcpSeqNumber,
    remote_last_ts: Option<Instant>,
    remote_endpoint: IpEndpoint,
    local_enpoint: IpEndpoint,
    rtte: smoltcp::socket::tcp::RttEstimator,
    state: TcpState,
    timer: smoltcp::socket::tcp::Timer,
    ack_delay_until: Option<Instant>,

    //passed down state
    remote_seq_no: TcpSeqNumber,
    remote_has_sack: bool,
    remote_mss: usize,
    remote_win_scale: Option<u8>,
    remote_win_shift: u8,
    remote_win_len: usize,
    local_rx_last_seq: Option<TcpSeqNumber>,
    local_seq_no: TcpSeqNumber,
    rx_fin_receive: bool,
    local_rx_dup_acks: u8,
    local_rx_last_ack: Option<TcpSeqNumber>,
    assembler: smoltcp::storage::Assembler,

    //Buffers are transfered via a vec for now where the indices and length should match the inner ringbuffer.
    rx_buffer: (Vec<u8>, usize, usize),
    tx_buffer: (Vec<u8>, usize, usize)
}

///State change after socket.dispatch
pub struct AfterDispatchState{
    was_reset: bool,
    neighbor_cache: (Vec<Option<(IpAddress, Neighbor)>>, Instant, usize),
    
    //shared state
    remote_last_ack: Option<TcpSeqNumber>,
    remote_last_win: u16,
    remote_last_seq: TcpSeqNumber,
    remote_last_ts: Option<Instant>,
    remote_endpoint: IpEndpoint,
    local_enpoint: IpEndpoint,
    local_seq_no: TcpSeqNumber,
    rtte: smoltcp::socket::tcp::RttEstimator,
    state: TcpState,
    timer: smoltcp::socket::tcp::Timer,
    ack_delay_until: Option<Instant>,
    tx_buffer: (Vec<u8>, usize, usize)
}


type Package = Vec<u8>;

pub enum StackRespons<'frame>{
    ControlPackage(EthernetPacket<'frame>),
    NoPackage,
    FinishedPackage(Package),
    ///emitted if the socket went into closed state 
    Closed,
}

pub struct StackContext<'a>{
    pub neighbor_cache: Vec<Option<(IpAddress, Neighbor)>>,
    pub ip_addr: [IpCidr; 1],
    pub tx_buffer: [u8; TX_BUFFER_SIZE],
    pub rx_buffer: [u8; RX_BUFFER_SIZE],
    pub socket_set_buffer: [Option<SocketSetItem<'a>>; 2],
}

impl<'a> StackContext<'a>{
    pub fn new(addr: IpEndpoint) -> Self{
	StackContext{
	    neighbor_cache: [None; NEIGHBOR_CACHE_SIZE].to_vec(),
	    ip_addr: [IpCidr::new(addr.addr, 8)],
	    tx_buffer: [0; TX_BUFFER_SIZE],
	    rx_buffer: [0; RX_BUFFER_SIZE],
	    socket_set_buffer: [None, None]
	}
    }
}

pub fn init_stack<'a>(
    in_context: &'a mut StackContext<'a>,
    out_context: &'a mut StackContext<'a>,
    addr: IpEndpoint
) -> (NetStack<'a>, NetStack<'a>){
    (NetStack::new(in_context, addr), NetStack::new(out_context, addr))
}

pub struct NetStack<'a>{
    ///Interface used for smoltcp
    pub iface: EthernetInterface<'a, WrapperDev>,

    pub socket_set: SocketSet<'a>,
    ///handle to own socket in socket set
    handle: SocketHandle,
}

impl<'a> NetStack<'a>{
    ///Creates some netstack, however, uninitialized, needs to be done via `initialize`.
    ///
    /// The own local address is `addr`
    pub fn new(
	context: &'a mut StackContext<'a>,
	addr: IpEndpoint
    ) -> Self{
	let neighbor_cache = NeighborCache::new(&mut context.neighbor_cache[..]);

	let device = WrapperDev{
	    receive_buffer: Vec::new(),
	    transmitt_buffer: Vec::new()
	};

	
	let iface = EthernetInterfaceBuilder::new(device)
            .ethernet_addr(EthernetAddress::default())
            .neighbor_cache(neighbor_cache)
            .ip_addrs(&mut context.ip_addr[..])
            .finalize();

	let mut server_socket = {
            let tcp_rx_buffer = TcpSocketBuffer::new(&mut context.rx_buffer[..]);
            let tcp_tx_buffer = TcpSocketBuffer::new(&mut context.tx_buffer[..]);
            TcpSocket::new(tcp_rx_buffer, tcp_tx_buffer)
	};

	server_socket.listen(addr).expect("Failed to set socket state to listen");

	let mut socket_set = SocketSet::new(&mut context.socket_set_buffer[..]);
	let server_handle = socket_set.add(server_socket);
	
	NetStack{
	    iface,
	    socket_set,
	    handle: server_handle
	}
    }
    
    pub fn copy_state(&mut self, other_stack: &mut NetStack<'a>){
	//Copy whole inner interface state
	other_stack.iface.inner.copy_state_into(&mut self.iface.inner);
	
	//Copy socket state
	let other_socket = other_stack.socket_set.get::<TcpSocket>(other_stack.handle);
	let mut this_socket = self.socket_set.get::<TcpSocket>(self.handle);

	this_socket.state = other_socket.state;
	this_socket.timer = other_socket.timer.clone();
	this_socket.rtte = other_socket.rtte.clone();
	this_socket.assembler = other_socket.assembler.copy_state();

	other_socket.rx_buffer.copy_into(&mut this_socket.rx_buffer);
	other_socket.tx_buffer.copy_into(&mut this_socket.tx_buffer);
	//this_socket.rx_buffer do not copy rx buffer since we don't want to receive here anyways
	//this_socket.rx_fin_received: bool,
	//this_socket.tx_buffer = other_socket.tx_buffer.clone();
	this_socket.timeout = other_socket.timeout.clone();
	this_socket.keep_alive = other_socket.keep_alive.clone();
	this_socket.hop_limit = other_socket.hop_limit.clone();
	this_socket.listen_address = other_socket.listen_address.clone();
	this_socket.local_endpoint = other_socket.local_endpoint.clone();
	this_socket.remote_endpoint = other_socket.remote_endpoint.clone();
	this_socket.local_seq_no = other_socket.local_seq_no.clone();
	this_socket.remote_seq_no = other_socket.remote_seq_no.clone();
	this_socket.remote_last_seq = other_socket.remote_last_seq.clone();
	this_socket.remote_last_ack = other_socket.remote_last_ack.clone();
	this_socket.remote_last_win = other_socket.remote_last_win.clone();
	this_socket.remote_win_shift = other_socket.remote_win_shift.clone();
	this_socket.remote_win_len = other_socket.remote_win_len.clone();
	this_socket.remote_win_scale = other_socket.remote_win_scale.clone();
	this_socket.remote_has_sack = other_socket.remote_has_sack.clone();
	this_socket.remote_mss = other_socket.remote_mss.clone();
	this_socket.remote_last_ts = other_socket.remote_last_ts.clone();
	this_socket.local_rx_last_seq = other_socket.local_rx_last_seq.clone();
	this_socket.local_rx_last_ack = other_socket.local_rx_last_ack.clone();
	this_socket.local_rx_dup_acks = other_socket.local_rx_dup_acks.clone();
	this_socket.ack_delay = other_socket.ack_delay.clone();
	this_socket.ack_delay_until = other_socket.ack_delay_until.clone();
    }

    fn create_after_process_state(
	iface: &EthernetInterface<'a, WrapperDev>,
	socket: &mut TcpSocket
    ) -> AfterProcessState{
	
	let mut new_state = AfterProcessState{
	    neighbor_cache: ([None; NEIGHBOR_CACHE_SIZE].to_vec(), Instant::from_millis(0), 0),//gets filled later 
	    //shared state
	    remote_last_ack: socket.remote_last_ack,
	    remote_last_win: socket.remote_last_win,
	    remote_last_seq: socket.remote_last_seq,
	    remote_last_ts: socket.remote_last_ts,
	    remote_endpoint: socket.remote_endpoint,
	    local_enpoint: socket.local_endpoint,
	    rtte: socket.rtte,
	    state: socket.state,
	    timer: socket.timer,
	    ack_delay_until: socket.ack_delay_until,

	    //passed down state
	    remote_seq_no: socket.remote_seq_no,
	    remote_has_sack: socket.remote_has_sack,
	    remote_mss: socket.remote_mss,
	    remote_win_scale: socket.remote_win_scale,
	    remote_win_shift: socket.remote_win_shift,
	    remote_win_len: socket.remote_win_len,
	    local_rx_last_seq: socket.local_rx_last_seq,
	    local_seq_no: socket.local_seq_no,
	    rx_fin_receive: socket.rx_fin_received,
	    local_rx_dup_acks: socket.local_rx_dup_acks,
	    local_rx_last_ack: socket.local_rx_last_ack,
	    assembler: socket.assembler.clone(),

	    //Buffers are transfered via a vec for now where the indices and length should match the inner ringbuffer.
	    rx_buffer: ([0 as u8; RX_BUFFER_SIZE].to_vec(), 0, 0),
	    tx_buffer: ([0 as u8; TX_BUFFER_SIZE].to_vec(), 0, 0)
	};

	//Copy neighbor cache state
	let (silence, thresold) = iface.inner.neighbor_cache.into_standalone(&mut new_state.neighbor_cache.0[..]);
	new_state.neighbor_cache.1 = silence;
	new_state.neighbor_cache.2 = thresold;

	//Copy tx and rx buffers
	let new_tx_state = socket.tx_buffer.into_standalone(&mut new_state.tx_buffer.0[..]);
	new_state.tx_buffer.1 = new_tx_state.0;
	new_state.tx_buffer.2 = new_tx_state.1;
	
	let new_rx_state = socket.rx_buffer.into_standalone(&mut new_state.rx_buffer.0[..]);
	new_state.rx_buffer.1 = new_rx_state.0;
	new_state.rx_buffer.2 = new_rx_state.1;
	
	new_state
    }

    pub fn load_after_process(&mut self, state: AfterProcessState){

	let (mut cache, inst, thres) = state.neighbor_cache;
	self.iface.inner.neighbor_cache.from_standalone(&mut cache[..], inst, thres);

	let mut socket = self.socket_set.get::<TcpSocket>(self.handle);
	
	    //shared state
	socket.remote_last_ack = state.remote_last_ack;
	socket.remote_last_win = state.remote_last_win;
	socket.remote_last_seq = state.remote_last_seq;
	socket.remote_last_ts = state.remote_last_ts;
	socket.remote_endpoint = state.remote_endpoint;
	socket.local_endpoint = state.local_enpoint;
	socket.rtte = state.rtte;
	socket.state = state.state;
	socket.timer = state.timer;
	socket.ack_delay_until = state.ack_delay_until;

	    //passed down state
	socket.remote_seq_no = state.remote_seq_no;
	socket.remote_has_sack = state.remote_has_sack;
	socket.remote_mss = state.remote_mss;
	socket.remote_win_scale = state.remote_win_scale;
	socket.remote_win_shift = state.remote_win_shift;
	socket.remote_win_len = state.remote_win_len;
	socket.local_rx_last_seq = state.local_rx_last_seq;
	socket.local_seq_no = state.local_seq_no;
	socket.rx_fin_received = state.rx_fin_receive;
	socket.local_rx_dup_acks = state.local_rx_dup_acks;
	socket.local_rx_last_ack = state.local_rx_last_ack;
	socket.assembler = state.assembler;

	//Buffers are transfered via a vec for now where the indices and length should match the inner ringbuffer.
	let (mut rxb, rxat, rxlen) = state.rx_buffer;
	let rxb = rxb.as_mut_slice().into();
	socket.rx_buffer.from_standalone(rxb, rxat, rxlen);
	let (mut txb, txat, txlen) = state.tx_buffer;
	let txb = txb.as_mut_slice().into();
	socket.tx_buffer.from_standalone(txb, txat, txlen);	
    }

    pub fn create_after_dispatch_state(
	iface: &EthernetInterface<'a, WrapperDev>,
	socket: &mut TcpSocket
    ) -> AfterDispatchState{
	let mut new_state = AfterDispatchState{
	    was_reset: socket.take_reset_state(),
	    neighbor_cache: ([None; NEIGHBOR_CACHE_SIZE].to_vec(), Instant::from_millis(0), 0),
	    remote_last_ack: socket.remote_last_ack,
	    remote_last_win: socket.remote_last_win,
	    remote_last_seq: socket.remote_last_seq,
	    remote_last_ts: socket.remote_last_ts,
	    remote_endpoint: socket.remote_endpoint,
	    local_enpoint: socket.local_endpoint,
	    local_seq_no: socket.local_seq_no,
	    rtte: socket.rtte,
	    state: socket.state,
	    timer: socket.timer,
	    ack_delay_until: socket.ack_delay_until,
	    tx_buffer: ([0 as u8; TX_BUFFER_SIZE].to_vec(), 0, 0),
	};

	let (silence, thresold) = iface.inner.neighbor_cache.into_standalone(&mut new_state.neighbor_cache.0[..]);
	new_state.neighbor_cache.1 = silence;
	new_state.neighbor_cache.2 = thresold;
	
	let new_tx_state = socket.tx_buffer.into_standalone(&mut new_state.tx_buffer.0[..]);
	new_state.tx_buffer.1 = new_tx_state.0;
	new_state.tx_buffer.2 = new_tx_state.1;


	new_state
    }

    pub fn load_after_dispatch_state(&mut self, state: AfterDispatchState){
	let (mut cache, inst, thres) = state.neighbor_cache;
	self.iface.inner.neighbor_cache.from_standalone(&mut cache[..], inst, thres);

	let mut socket = self.socket_set.get::<TcpSocket>(self.handle);
	if state.was_reset{
	    socket.reset();
	}
	//shared state
	socket.remote_last_ack = state.remote_last_ack;
	socket.remote_last_win = state.remote_last_win;
	socket.remote_last_seq = state.remote_last_seq;
	socket.remote_last_ts = state.remote_last_ts;
	socket.remote_endpoint = state.remote_endpoint;
	socket.local_endpoint = state.local_enpoint;
	socket.local_seq_no = state.local_seq_no;
	socket.rtte = state.rtte;
	socket.state = state.state;
	socket.timer = state.timer;
	socket.ack_delay_until = state.ack_delay_until;
	
	let (mut txb, txat, txlen) = state.tx_buffer;
	let txb = txb.as_mut_slice().into();
	socket.tx_buffer.from_standalone(txb, txat, txlen);	
    }
    
    pub fn process_ingress<'b>(&mut self, rx_buffer: &'b Vec<u8>, timestamp: Instant) -> (AfterProcessState, StackRespons<'b>) {
	//similar to EthernetInterface::socket_ingress, we consume the rx token at the inner timestamp

	//Check if we have to respond, by processing the frame on the netstack
	let repl = self.iface.inner.process_ethernet(&mut self.socket_set, timestamp, rx_buffer).map_err(|err| {
            print!("NETDEBUG: cannot process ingress packet: {}", err);
            print!("NETDEBUG: packet dump follows:\n{}",
                   PrettyPrinter::<EthernetFrame<&[u8]>>::new("", &rx_buffer));
            err
        });

	if let Ok(control_package) = repl{
	    //Process control package according to the socket_ingress function
	    match control_package{
		Some(p) => {
		    println!("ingress needs to send control package");
		    let mut socket = self.socket_set.get::<TcpSocket>(self.handle);
		    (Self::create_after_process_state(&self.iface, &mut socket), StackRespons::ControlPackage(p))
		},
		None => {
		    println!("Got reply but no control package, try to take one out of the buffer");
		    let mut socket = self.socket_set.get::<TcpSocket>(self.handle);

		    if let Ok(buffer) = socket.recv(|buffer| ((buffer.len(), buffer.to_vec()))){
			//If the buffer is empty this is no real package, therefore return NoPackage
			if buffer.len() == 0{
			    (Self::create_after_process_state(&self.iface, &mut socket), StackRespons::NoPackage)
			}else{
			    (Self::create_after_process_state(&self.iface, &mut socket), StackRespons::FinishedPackage(buffer))
			}
		    }else{
			//check if we closed, in that case, emmit the closed respons, otherwise we just can't do anything
			if socket.state() == TcpState::Closed{
			    println!("Changed into Closed state!");
			    (Self::create_after_process_state(&self.iface, &mut socket), StackRespons::Closed)
			}else{
			    (Self::create_after_process_state(&self.iface, &mut socket), StackRespons::NoPackage)
			}
		    }
		}
	    }
	}else{
	    println!("Got No package at all");
	    let mut socket = self.socket_set.get::<TcpSocket>(self.handle);
	    (Self::create_after_process_state(&self.iface, &mut socket), StackRespons::NoPackage)
	}
	//TODO if no control package was send, check if we can take out a whole new package out of the rx_buffer of our
	//socket.
    }

    pub fn on_socket<F, R>(&mut self, mut f: F) -> R where F: FnMut(&mut TcpSocket) -> R{
	let mut socket = self.socket_set.get::<TcpSocket>(self.handle);
	f(&mut socket)
    }

    ///Just dispatches the socket witha  token and package, without processing the ethernet stack.
    /// Used to send control packages
    pub fn direct_dispatch<Tx>(&mut self, token: Tx, clock: Instant, package: EthernetPacket) -> AfterDispatchState where Tx: TxToken{
	//Unwrap self so we dont have to borrow everything all the time
	let &mut Self{ref mut iface, ref mut socket_set, ref mut handle} = self;
	let mut socket = socket_set.get::<TcpSocket>(*handle);
	
	if let Err(e) = iface.inner.dispatch(token, clock, package){
	    println!("NETDEBUG: Failed to send control package!");
	}
	
	Self::create_after_dispatch_state(&iface, &mut socket)
    }
    
    pub fn process_egress<TX>(&mut self, caps: &DeviceCapabilities, token: TX, timestamp: Instant) -> AfterDispatchState where TX: TxToken{
	let mut caps = caps.clone();
	caps.max_transmission_unit -= EthernetFrame::<&[u8]>::header_len();


	//Unwrap self so we dont have to borrow everything all the time
	let &mut Self{ref mut iface, ref mut socket_set, ref mut handle} = self;
	

	let mut socket = socket_set.get::<TcpSocket>(*handle);
	if !socket.meta.egress_permitted(timestamp, |ip_addr|{
	    iface.inner.has_neighbor(&ip_addr, timestamp)
	}){
	    println!("Egress not permitted!");
	    return Self::create_after_dispatch_state(&iface, &mut socket);
	}

	let mut neighbor_addr = None;
        let mut device_result = Ok(());

	//Dispatch on the socket
        let socket_result = socket.dispatch(timestamp, &caps, |response|{
	    let response = IpPacket::Tcp(response);
            neighbor_addr = Some(response.ip_repr().dst_addr());
            let response = EthernetPacket::Ip(response);
            device_result = iface.inner.dispatch(token, timestamp, response);
            device_result
	});

        match (device_result, socket_result) {
            (Err(Error::Exhausted), _) => {
		println!("Socket exhausted");
		// nowhere to transmit
	    },     
            (Ok(()), Err(Error::Exhausted)) => {
		println!("Device exhausted");
		// nothing to transmit
	    },   
            (Err(Error::Unaddressable), _) => {
                // `NeighborCache` already takes care of rate limiting the neighbor discovery
                // requests from the socket. However, without an additional rate limiting
                // mechanism, we would spin on every socket that has yet to discover its
                // neighboor.
                socket.meta.neighbor_missing(timestamp,
						   neighbor_addr.expect("non-IP response packet"));
                println!("neighbor was missing");
            }
            (Err(err), _) | (_, Err(err)) => {
                println!("{}: cannot dispatch egress packet: {}",
                           socket.meta.handle, err);
                //return Err(err)
            }
            (Ok(()), Ok(())) => {}, //emitted_any = true
        }

	Self::create_after_dispatch_state(&iface, &mut socket)
    }
}

pub struct WrapperDev{
    pub receive_buffer: Vec<Vec<u8>>,
    pub transmitt_buffer: Vec<Vec<u8>>,
}

impl<'a> smoltcp::phy::Device<'a> for WrapperDev {
    type RxToken = WrapperRxToken;
    type TxToken = WrapperTxToken<'a>;
    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {

	panic!("Called receive on the dummy!");
	/*
	println!("---Wrapper receive");
	if self.receive_buffer.len() > 0{
	    let buffer = self.receive_buffer.remove(0);
	    None
	}else{
	    None
	}
	 */
    }

    fn transmit(&'a mut self) -> Option<Self::TxToken> {
	panic!("Called transmit on the dummy!")
    }

    fn capabilities(&self) -> DeviceCapabilities {
	//println!("Called capabilities on the dummy");
	let mut caps = DeviceCapabilities::default();
        caps.max_transmission_unit = 1536;
        caps.max_burst_size = Some(1);
	caps
    }
}

pub struct WrapperRxToken{
    token: Vec<u8>
}

impl phy::RxToken for WrapperRxToken {
    fn consume<R, F>(mut self, _timestamp: Instant, f: F) -> smoltcp::Result<R>
        where F: FnOnce(&mut [u8]) -> smoltcp::Result<R>
    {
	f(&mut self.token)
    }
}

pub struct WrapperTxToken<'a>{
    buffer: &'a mut Vec<u8>
}

impl<'a> phy::TxToken for WrapperTxToken<'a> {
    fn consume<R, F>(mut self, _timestamp: Instant, len: usize, f: F) -> smoltcp::Result<R>
        where F: FnOnce(&mut [u8]) -> smoltcp::Result<R>
    {
	//Resize the buffer in the transmitt queue
	self.buffer.resize(len, 0 as u8);
	//Fill it with the "to transmitt" data
	f(&mut self.buffer)
    }
}

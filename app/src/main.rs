use net::shared_loopback::Bridge;
use net::smoltcp;

use std::sync::Arc;
use std::sync::Mutex;
use std::thread::JoinHandle;

use std::collections::BTreeMap;

use smoltcp::socket::TcpState;
use smoltcp::wire::{EthernetAddress, IpAddress, IpCidr};
use smoltcp::iface::{NeighborCache, EthernetInterfaceBuilder};
use smoltcp::socket::{SocketSet, TcpSocket, TcpSocketBuffer};
use smoltcp::time::{Duration, Instant};


extern crate simple_logger;

fn start_client(device: Bridge, stop_flag: Arc<Mutex<bool>>) -> JoinHandle<()>{
    std::thread::spawn(move ||{
	let mut cache = [None; 128];
	let neighbor_cache = NeighborCache::new(&mut cache[..]);
	
	let mut ip_addrs = [IpCidr::new(IpAddress::v4(127, 0, 0, 1), 8)];
	println!("Created iface");
	let mut iface = EthernetInterfaceBuilder::new(device)
            .ethernet_addr(EthernetAddress::default())
            .neighbor_cache(neighbor_cache)
            .ip_addrs(&mut ip_addrs[..])
            .finalize();

	let client_socket = {
            // It is not strictly necessary to use a `static mut` and unsafe code here, but
            // on embedded systems that smoltcp targets it is far better to allocate the data
            // statically to verify that it fits into RAM rather than get undefined behavior
            // when stack overflows.
            static mut TCP_SERVER_RX_DATA: [u8; 1024] = [0; 1024];
            static mut TCP_SERVER_TX_DATA: [u8; 1024] = [0; 1024];
            let tcp_rx_buffer = TcpSocketBuffer::new(unsafe { &mut TCP_SERVER_RX_DATA[..] });
            let tcp_tx_buffer = TcpSocketBuffer::new(unsafe { &mut TCP_SERVER_TX_DATA[..] });
            TcpSocket::new(tcp_rx_buffer, tcp_tx_buffer)
	};
	
	let mut set_cache = [None];
	let mut socket_set = SocketSet::new(&mut set_cache[..]);
	let client_handle = socket_set.add(client_socket);

	//Connect client to server
	socket_set.get::<TcpSocket>(client_handle).connect(
	    (IpAddress::v4(127, 0, 0, 2), 1337),
            (IpAddress::Unspecified, 65000)
	    //(IpAddress::v4(127, 0, 0, 1), 1337)
	).expect("Failed to issue connect");

	let mut clock = Instant::from_millis(0);
	let mut send_next = true;
	let mut num_sends = 0;
	println!("Start client");
	//now loop, if the connection was successful at some point, start sending messages
	while num_sends < 5{

	    if *stop_flag.lock().unwrap(){
		println!("Got end signal");
		break;
	    }

	    println!("start poll");
	    match iface.poll(&mut socket_set, clock){
		Ok(_) => {},
		Err(_e) => {}
	    }
	    println!("End poll");
	    
	    if socket_set.get::<TcpSocket>(client_handle).state() == TcpState::Established{
		if send_next{
		    if let Err(e) = socket_set.get::<TcpSocket>(client_handle).send_slice(b"Hello from client"){
			println!("failed to send {}", e);
		    }else{
			num_sends += 1;
			send_next = false;
		    }
		}else{
		    if let Ok(resp) = socket_set.get::<TcpSocket>(client_handle).recv(|buffer| (buffer.len(), buffer.to_vec())){
			if resp.len() > 0{
			    println!("=====Got respons: {}=======", core::str::from_utf8(&resp).unwrap());
			}else{
			    println!("=====Got Nothing  =======");
			}
			send_next = true;
		    }else{
			println!("Could not get respons");
		    }
		}
	    }else{
		println!("client not yet connected");
	    }

	    
	    match iface.poll_delay(&socket_set, clock) {
		Some(Duration { millis: 0 }) => {},
		Some(delay) => {
                    clock += delay;
		},
		None => clock += Duration::from_millis(1)
            }
	    std::thread::sleep(std::time::Duration::from_secs_f32(0.5));
	}

	println!("=====Client Close=====");
	socket_set.get::<TcpSocket>(client_handle).close();
	while socket_set.get::<TcpSocket>(client_handle).state() != TcpState::Closed{
	    match iface.poll(&mut socket_set, clock){
		Ok(_) => {},
		Err(_e) => {}
	    }

	    match iface.poll_delay(&socket_set, clock) {
		Some(Duration { millis: 0 }) => {},
		Some(delay) => {
                    clock += delay;
		},
		None => clock += Duration::from_millis(1)
            }
	    std::thread::sleep(std::time::Duration::from_secs_f32(0.5));
	}
    })
}

fn main() {
    //init logger
    simple_logger::SimpleLogger::new().init().unwrap();
    
    let (client_device, server_device) = net::shared_loopback::new_bridge();
    
    let stop_flag = Arc::new(Mutex::new(false));
    //Start client thread

    //Start server "app"

    let _client_handle = start_client(client_device, stop_flag.clone());
    
    
    let _app_handle = std::thread::spawn(move ||{
	net::network_application(server_device);
    });

    loop{
	
    }
}
